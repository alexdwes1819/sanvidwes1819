<?php
session_start();
if (isset($_GET["language"]))
    $language = trim(strip_tags($_GET["language"]));
else
{
    if (isset($_SESSION["language"]))
        $language = $_SESSION["language"];
    else
        $language = "es_ES";
}
$_SESSION["language"] = $language;

$language .= ".utf8";
$numComentarios = 2;

setlocale(LC_ALL, $language);

bindtextdomain("messages", "../locale");

bind_textdomain_codeset("messages", "UTF-8");

textdomain("messages");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= _('Clase de DWES') ?></title>
</head>
<body>
<p>
<a href="<?= $_SERVER["PHP_SELF"]; ?>?language=en_GB">
    English</a> -
<a href="<?= $_SERVER["PHP_SELF"]; ?>?language=es_ES">
    Español</a>
</p>
    <p><?= $numComentarios . ' ' . ngettext('comentario', 'comentarios', $numComentarios) ?> </p>
    <p><?= _('Hola Mundo'); ?></p>
    <p><?= _('adios!!'); ?></p>
</body>
</html>