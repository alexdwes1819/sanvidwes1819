<?php

use dwes\app\repository\UsuarioDB;
use dwes\core\App;
use dwes\core\Request;
use dwes\core\Router;

try {
    require '../core/bootstrap.php';

    Router::load('../app/routes.php');

    if (isset($_SESSION['usuario']))
    {
        $usuario = App::getRepository(UsuarioDB::class)->find(
            $_SESSION['usuario']
        );
    }
    else
        $usuario = null;

    App::bind('user', $usuario);

    App::get('router')->direct(Request::uri(), Request::method());
}
catch(Exception $exception)
{
    die($exception->getMessage());
}