<?php

namespace dwes\core;

class Security
{
    private static function getRoleNumber(
        string $roleBuscado, array $roles) : int
    {
        foreach($roles as $role=>$valor)
        {
            if ($roleBuscado === $role)
                return $valor;
        }

        return -1;
    }

    public static function isUserGranted(string $role) : bool
    {
        if ($role === 'ROLE_ANONIMO')
            return true;

        $user = App::get('user');
        $roles = App::get('config')['security']['roles'];
        $valorRoleUsuario = $roles[$user->getRole()];
        $valorRole = $roles[$role];

        if (is_null($user) === true || $valorRoleUsuario < $valorRole)
            return false;

        return true;
    }
}