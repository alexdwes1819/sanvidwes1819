<?php

namespace dwes\core;

use dwes\core\database\Connection;
use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class App
{
    private static $container = [];

    public static function bind(string $key, $value)
    {
        static::$container[$key] = $value;
    }

    public static function get(string $key)
    {
        if (! array_key_exists($key, static::$container))
            throw new Exception("No se ha encontrado la clave $key en el contenedor");

        return static::$container[$key];
    }

    public static function getConnection()
    {
        if (! array_key_exists('connection', static::$container))
            static::$container['connection'] = Connection::make();

        return static::$container['connection'];
    }

    public static function getRepository(string $className)
    {
        if (! array_key_exists($className, static::$container))
            static::$container[$className] = new $className();

        return static::$container[$className];
    }

    public static function getLogger()
    {
        $config = App::get('config');

        if (! array_key_exists('logger', static::$container))
        {
            static::$container['logger'] = new Logger(
                $config['logger']['name']);

            static::$container['logger']->pushHandler(
                new StreamHandler(
                    $config['logger']['file'],
                    $config['logger']['level'])
            );
        }

        return static::$container['logger'];
    }
}