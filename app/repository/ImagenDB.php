<?php

namespace dwes\app\repository;

use dwes\core\database\QueryBuilder;
use dwes\app\entity\Imagen;

class ImagenDB extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(
            'imagen',
            Imagen::class);
    }
}