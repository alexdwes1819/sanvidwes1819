<?php

namespace dwes\app\exceptions;

use Exception;
use Throwable;

class ValidationException extends Exception
{
    public function __construct(
        string $message = "Excepción de validación", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}