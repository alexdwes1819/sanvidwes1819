<?php

use dwes\core\App;

$router = App::get('router');
$router->get('', 'PagesController@inicio');
$router->get('about', 'PagesController@about');
$router->get('back-gallery', 'ImagenController@listar', 'ROLE_ADMIN');
$router->get('back-gallery/:id', 'ImagenController@show', 'ROLE_USER');
$router->get('delete-image', 'ImagenController@elimina', 'ROLE_USER');
$router->get('gallery', 'ImagenController@galeria');
$router->post('images/new', 'ImagenController@nueva', 'ROLE_USER');
$router->get('login', 'AuthController@login');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('idioma/:idioma', 'InternacionalizationController@cambiaIdioma', 'ROLE_USER');
