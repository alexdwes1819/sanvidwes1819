<?php

namespace dwes\app\controllers;

use dwes\app\repository\UsuarioDB;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\Response;

class AuthController
{
    public function login()
    {
        $loginError = FlashMessage::get('login-error');

        Response::renderView(
            'login',
            compact(
                'loginError'
            )
        );
    }

    public function checkLogin()
    {
        if (!isset($_POST['username']) || !isset($_POST['password']) ||
            empty($_POST['username']) || empty($_POST['password']))
        {
            FlashMessage::set('login-error', 'Debes introducir usuario y password');
            App::get('router')->redirect('login');
        }

        $usuarios = App::getRepository(UsuarioDB::class)->findBy(
            [
                'username' => $_POST['username'],
                'password' => $_POST['password']
            ]
        );

        if (empty($usuarios))
        {
            FlashMessage::set('login-error', 'Nombre de usuario y/o password incorrecto');
            App::get('router')->redirect('login');
        }

        $_SESSION['usuario'] = $usuarios[0]->getId();
        App::get('router')->redirect('back-gallery');
    }

    public function logout()
    {
        $_SESSION['usuario'] = null;

        unset($_SESSION['usuario']);

        App::get('router')->redirect('');
    }

    public function unauthorized()
    {
        header(
            'HTTP/1.1 403 Forbidden',
            true,
            403);
        Response::renderView('403');
    }
}