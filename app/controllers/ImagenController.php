<?php

namespace dwes\app\controllers;

use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\Response;
use dwes\app\entity\Imagen;
use dwes\app\repository\ImagenDB;
use dwes\app\repository\UsuarioDB;
use PDOException;

class ImagenController
{
    public function listar()
    {
        $imagenes = App::getRepository(ImagenDB::class)
            ->findAll();

        $error = FlashMessage::get('error');
        $nuevaImagen = FlashMessage::get('nueva-imagen');

        $data = compact('imagenes', 'error', 'nuevaImagen');

        Response::renderView(
            'back-gallery',
            $data
        );
    }

    /**
     * @throws \Exception
     */
    public function nueva()
    {
        $imagen = new Imagen();

        $imagen->setField($_FILES['imagen'])
            ->setAcceptedTypes(
                [
                    'image/gif',
                    'image/png',
                    'image/jpeg'
                ]
            )
            ->setTargetDirectory('./img/gallery/')
            ->setError();

        if ($imagen->executeUpload() === true)
        {
            $pdo = App::getConnection();
            try
            {
                $pdo->beginTransaction();

                $imagen->setAncho($_POST['ancho']);
                $imagen->setNombre($imagen->getFileName());
                $imagen->setDescripcion($_POST['descripcion']);
                $imagen->setUser(1);

                App::getRepository(ImagenDB::class)->insert($imagen);

                $sql = 'UPDATE usuario SET images=images+1 WHERE id=1';
                App::getRepository(UsuarioDB::class)->executeSql($sql);

                $pdo->commit();

                App::getLogger()->addWarning(
                    'Nueva imagen creada ' . $imagen->getNombre());

                FlashMessage::set(
                    'nueva-imagen', 'Se ha insertado la imagen ' . $imagen->getNombre());
            }
            catch(PDOException $exception)
            {
                $pdo->rollBack();

                FlashMessage::set(
                    'error', $exception->getMessage());
            }
        }
        else
        {
            FlashMessage::set(
                'error', $imagen->getLastError());
        }

        App::get('router')->redirect('back-gallery');
    }

    public function elimina()
    {
        $idImagen = $_GET['id'];

        if (isset($idImagen))
        {
            App::getRepository(ImagenDB::class)->delete($idImagen);

            $resultado = true;
        }
        else
            $resultado = false;

        echo json_encode($resultado);
    }

    public function galeria()
    {
        $imagenes = App::getRepository(ImagenDB::class)->findAll();

        $data = compact('imagenes');

        Response::renderView(
            'gallery',
            $data
        );
    }

    public function show($id)
    {
        $imagen = App::getRepository(ImagenDB::class)->find($id);

        $data = compact('imagen');

        Response::renderView(
            'back-gallery-show',
            $data
        );
    }
}