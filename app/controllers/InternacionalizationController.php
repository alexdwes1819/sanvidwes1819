<?php

namespace dwes\app\controllers;

use dwes\core\App;

class InternacionalizationController
{
    public function cambiaIdioma(string $idioma)
    {
        switch($idioma)
        {
            case 1:
                $_SESSION['idioma'] = 'es_ES';
                break;
            case 2:
                $_SESSION['idioma'] = 'en_GB';
                break;

            default:
                $_SESSION['idioma'] = 'es_ES';
                break;
        }

        App::get('router')->redirect('');
    }
}