<?php

namespace dwes\app\controllers;


use dwes\core\Response;

class PagesController
{
    public function about()
    {
        Response::renderView(
            'about'
        );
    }

    public function inicio()
    {
        Response::renderView(
            'index'
        );
    }
}