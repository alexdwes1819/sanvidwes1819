<?php

namespace dwes\app\entity;

use dwes\app\utils\UploadFile;

class Imagen implements IEntity
{
    use UploadFile;

    public const RUTA_IMGS = 'img/gallery/';

    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $ancho;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $descripcion;
    /**
     * @var int
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return int
     */
    public function getAncho(): int
    {
        return $this->ancho;
    }

    /**
     * @param int $ancho
     * @return Imagen
     */
    public function setAncho(int $ancho): Imagen
    {
        $this->ancho = $ancho;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Imagen
     */
    public function setNombre(string $nombre): Imagen
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return Imagen
     */
    public function setDescripcion(string $descripcion=null): Imagen
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @param int $user
     * @return Imagen
     */
    public function setUser(int $user): Imagen
    {
        $this->user = $user;
        return $this;
    }


    public function getUrlImagen()
    {
        return self::RUTA_IMGS . $this->getNombre();
    }

    public function toArray()
    {
        return [
            'ancho' => $this->ancho,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'user' => $this->user
        ];
    }
}