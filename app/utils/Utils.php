<?php

namespace dwes\app\utils;

class Utils
{
    public static function isActiveMenu(string $menuOption) : bool
    {
        return (strpos($_SERVER['REQUEST_URI'], $menuOption) !== false);
    }
}