<?php
namespace dwes\app\utils;

trait UploadFile
{
    /**
     * @var array
     */
    private $field;
    /**
     * @var string []
     */
    private $acceptedTypes;
    /**
     * @var string
     */
    private $targetDirectory;
    /**
     * @var string
     */
    private $error;
    /**
     * @var string
     */
    private $fileName;

    /**
     * @param string $error
     * @return UploadFile
     */
    public function setError(string $error=null)
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @param array $field
     * @return UploadFile
     */
    public function setField(array $field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @param string[] $acceptedTypes
     * @return UploadFile
     */
    public function setAcceptedTypes(array $acceptedTypes)
    {
        $this->acceptedTypes = $acceptedTypes;
        return $this;
    }

    /**
     * @param string $targetDirectory
     * @return UploadFile
     */
    public function setTargetDirectory(string $targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
        return $this;
    }


    public function getLastError()
    {
        return $this->error;
    }

    private function checkError() : bool
    {
        if ($this->field['error'] !== UPLOAD_ERR_OK)
        {
            switch ($this->field['error'])
            {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    $this->error = 'excedido tamaño máximo de fichero';
                    break;

                case UPLOAD_ERR_CANT_WRITE:
                    $this->error = 'No se ha podido escribir en el directorio temporal';
                    break;

                case UPLOAD_ERR_NO_FILE:
                    $this->error = 'No se ha subido ningún fichero';
                    break;

                case UPLOAD_ERR_PARTIAL:
                    $this->error = 'No se ha podido subir el fichero por completo';
                    break;

                default:
                    $this->error = null;
                    break;
            }

            return false;
        }

        return true;
    }

    private function checkTypes() : bool
    {
        if (in_array($this->field['type'], $this->acceptedTypes) === true)
            return true;

        $this->error = 'Tipo de imagen no soportado';

        return false;
    }

    private function checkUploadOk() : bool
    {
        if (is_uploaded_file ( $this->field['tmp_name']) === false)
        {
            $this->error = 'error: posible ataque (Arbitrary file disclosure through PHP file upload)';

            return false;
        }

        return true;
    }

    private function getTargetFile(string $name) : string
    {
        return $this->targetDirectory . $name;
    }

    public function getFileName() : string
    {
        $this->fileName = $this->field['name'];
        $target = $this->getTargetFile($this->fileName);

        if (is_file($target) === true)
        {
            $idUnico = time();
            $this->fileName = $idUnico . $this->fileName;
        }

        return $this->fileName;
    }

    private function moveFile(string $name) : bool
    {
        $target = $this->getTargetFile($name);
        if (move_uploaded_file(
                $this->field['tmp_name'],
                $target) === false)
        {
            $this->error = "No se ha podido mover el fichero a $target";

            return false;
        }

        return true;
    }

    public function executeUpload() : bool
    {
        if ($this->checkError() === false
            || $this->checkTypes() === false
            || $this->checkUploadOk() === false)
            return false;

        $fileName = $this->getFileName();

        return $this->moveFile($fileName);
    }
}