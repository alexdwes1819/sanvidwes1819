<section class="contact-page-area section-gap">
    <div class="container">
        <?php if (isset($error) && !empty($error)) : ?>
            <div class="row">
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (isset($nuevaImagen) && !empty($nuevaImagen)) : ?>
            <div class="row">
                <div class="alert alert-success" role="alert">
                    <?= $nuevaImagen ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-lg-12">
                <form class="form-area contact-form text-right"
                      id="myForm"
                      action="/images/new"
                      method="post"
                      enctype="multipart/form-data"
                >
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <input name="imagen" class="common-input mb-20 form-control" type="file">
                        </div>
                        <div class="col-lg-12 form-group">
                            <input name="ancho" class="common-input mb-20 form-control" type="number" placeholder="Ancho">
                        </div>
                        <div class="col-lg-12 form-group">
                            <input name="descripcion" class="common-input mb-20 form-control" type="text" placeholder="Descripción">
                        </div>
                        <div class="col-lg-12">
                            <div class="alert-msg" style="text-align: left;"></div>
                            <input type="submit" class="genric-btn primary" style="float: right;" value="Enviar">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <tr>
                        <th>Operaciones</th>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Ancho</th>
                        <th>Descripción</th>
                    </tr>
                    <?php foreach ($imagenes as $imagen) : ?>
                        <tr>
                            <td><a class="delete" id="<?= $imagen->getId() ?>" href="#">Delete</a></td>
                            <td><?= $imagen->getId() ?></td>
                            <td>
                                <img width="100" class="img-responsive" src="<?= $imagen->getUrlImagen() ?>"
                                     alt="<?= $imagen->getDescripcion() ?>">
                            </td>
                            <td><?= $imagen->getAncho() ?></td>
                            <td><?= $imagen->getDescripcion() ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
    </div>
</section>