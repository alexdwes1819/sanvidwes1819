<section class="contact-page-area section-gap">
    <div class="container">
        <form class="form-area contact-form text-right"
              id="myForm"
              action="/check-login"
              method="post"
        >
            <div class="row">
                <div class="col-lg-12 form-group">
                    <input name="username" class="common-input mb-20 form-control" type="text"  placeholder="Usuario">
                </div>
                <div class="col-lg-12 form-group">
                    <input name="password" class="common-input mb-20 form-control" type="password" placeholder="Password">
                </div>
                <div class="col-lg-12">
                    <div class="alert-msg" style="text-align: left;"></div>
                    <input type="submit" class="genric-btn primary" style="float: right;" value="Enviar">
                </div>
            </div>
        </form>
        <?php if (isset($loginError) && !empty($loginError)) : ?>
            <div class="row">
                <div class="alert alert-danger" role="alert">
                    <?= $loginError ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>