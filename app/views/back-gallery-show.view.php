<section class="contact-page-area section-gap">
    <div class="container">
        <?php if (isset($error)) : ?>
            <div class="row">
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <tr>
                        <th>Operaciones</th>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Ancho</th>
                        <th>Descripción</th>
                    </tr>
                    <tr>
                        <td><a class="delete" id="<?= $imagen->getId() ?>" href="#">Delete</a></td>
                        <td><?= $imagen->getId() ?></td>
                        <td>
                            <img width="100" class="img-responsive" src="<?= $imagen->getUrlImagen() ?>"
                                 alt="<?= $imagen->getDescripcion() ?>">
                        </td>
                        <td><?= $imagen->getAncho() ?></td>
                        <td><?= $imagen->getDescripcion() ?></td>
                    </tr>
                </table>
            </div>
    </div>
</section>