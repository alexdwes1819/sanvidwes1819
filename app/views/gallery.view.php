<!-- start banner Area -->
			<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Gallery				
							</h1>	
							<p class="text-white link-nav"><a href="../../public/index.php">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="../../public/index.php"> Gallery</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	
				
			<!-- Start gallery Area -->
			<section class="gallery-area section-gap">
				<div class="container">
					<div class="row">
                        <?php foreach($imagenes as $imagen) : ?>
                            <div class="col-lg-<?= $imagen->getAncho() ?>">
                                <a href="<?= $imagen->getUrlImagen() ?>" class="img-gal">
                                    <div class="single-imgs relative">
                                        <div class="overlay overlay-bg"></div>
                                        <div class="relative">
                                            <img class="img-fluid" src="<?= $imagen->getUrlImagen() ?>" alt="<?= $imagen->getDescripcion() ?>">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
					</div>
				</div>	
			</section>
			<!-- End gallery Area -->
													

			<!-- Start cta-two Area -->
			<section class="cta-two-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 cta-left">
							<h1>Not Yet Satisfied with our Trend?</h1>
						</div>
						<div class="col-lg-4 cta-right">
							<a class="primary-btn wh" href="#">view our blog</a>
						</div>
					</div>
				</div>	
			</section>
			<!-- End cta-two Area -->